package com.example.data.repository;

import static com.example.data.model.Next.LIMIT;
import static com.example.data.model.Next.OFFSET;

import android.content.Context;
import android.net.Uri;

import com.example.data.api.Api;
import com.example.data.api.PokemonApi;
import com.example.data.model.Next;
import com.example.data.model.remote.Pokemon;
import com.example.data.model.PokemonDetail;
import com.example.data.model.remote.PokemonDetailsResponse;
import com.example.data.model.PokemonModel;
import com.example.data.model.remote.PokemonResponse;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import io.reactivex.Single;

public final class PokemonRepository {
    private static final String TAG = "PokemonRepository";

    private PokemonApi mPokemonApi;

    private static PokemonRepository INSTANCE;

    public static PokemonRepository getInstance( Context context){
        if(INSTANCE == null){
            INSTANCE = new PokemonRepository(context);
        }

        return INSTANCE;
    }

    public PokemonRepository(Context context) {
        mPokemonApi = new Api(context);
    }

    public Single<List<PokemonModel>> getPokemonList(@Nullable Next next) {
        final int offset = next != null ? next.getOffset() : Next.STARTING_OFFSET;
        final int limit = next != null ? next.getLimit() : Next.LIMIT_VALUE;
        return mPokemonApi.getPokemon(offset, limit)
                .flatMap(this::mappingPokemon)
                .flattenAsObservable(list -> list)
                .flatMapSingle( pokemon -> getPokemonDetails(pokemon.getPokemon().getName())
                        .map(pokemonDetails -> addingPokemonDetails(pokemon, pokemonDetails))
                )
                .toList();
    }

    private Single<PokemonDetailsResponse> getPokemonDetails(String name) {
        return mPokemonApi.getPokemonDetails(name);
    }

    private PokemonModel addingPokemonDetails(PokemonModel pokemon,
                                              PokemonDetailsResponse pokemonDetails) {
        pokemon.setPokemonDetail(
                new PokemonDetail(
                        pokemonDetails.getSpirites(),
                        pokemonDetails.getSpecies(),
                        pokemonDetails.getStats(),
                        pokemonDetails.getTypes()
                )
        );
        return pokemon;
    }

    private Single<List<PokemonModel>> mappingPokemon(PokemonResponse response) {
        final List<PokemonModel> pokemonList = new ArrayList<>();
        if (response != null && response.getResults().size() > 0) {
            for (Pokemon p : response.getResults()) {
                pokemonList.add(new PokemonModel(
                        getNextObject(response.getNext()),
                        new Pokemon(
                                p.getName(),p.getUrl()
                        ), null, response.getCount()));
            }
        }
        return Single.just(pokemonList);
    }

    private Next getNextObject(String next) {
        if(next == null) {
            return null;
        }
        final Uri uri = Uri.parse(next);
        return new Next(
                Integer.valueOf(uri.getQueryParameter(OFFSET)),
                Integer.valueOf(uri.getQueryParameter(LIMIT)));
    }


}
