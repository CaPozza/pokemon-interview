package com.example.data.api;

import android.content.Context;

import com.example.data.BuildConfig;
import com.example.data.model.remote.PokemonDetailsResponse;
import com.example.data.model.remote.PokemonResponse;
import com.facebook.stetho.okhttp3.StethoInterceptor;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class Api implements PokemonApi {
    private static final String TAG = "Api";
    private PokemonApi mPokemonApi;
    private Context mContext;
   public Api(Context context) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG) interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        final OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .cache(null);

        if (BuildConfig.DEBUG) builder.addNetworkInterceptor(new StethoInterceptor());

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(builder.build())
                .baseUrl(BuildConfig.POKEMON_API)
                .build();
        mPokemonApi = retrofit.create(PokemonApi.class);
        this.mContext = context;
    }

    @Override
    public Single<PokemonResponse> getPokemon(int offset, int limit) {
        return mPokemonApi.getPokemon(offset, limit).subscribeOn(Schedulers.io());
    }

    @Override
    public Single<PokemonDetailsResponse> getPokemonDetails(String name) {
        return mPokemonApi.getPokemonDetails(name).subscribeOn(Schedulers.io());
    }
}
