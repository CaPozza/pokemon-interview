package com.example.data.api;

import com.example.data.model.remote.PokemonDetailsResponse;
import com.example.data.model.remote.PokemonResponse;


import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface  PokemonApi {
    @Headers({"Content-Type:application/json", })
    @GET("v2/pokemon/")
    Single<PokemonResponse> getPokemon(@Query("offset") int offset,
                                              @Query("limit") int limit);

    @Headers({"Content-Type:application/json", })
    @GET("v2/pokemon/{POKEMON_NAME}")
    Single<PokemonDetailsResponse> getPokemonDetails(@Path("POKEMON_NAME") String name);
}
