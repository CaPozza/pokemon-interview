package com.example.data.model.remote;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Types {
    @SerializedName("slot")
    @Expose
    private String slot;

    @SerializedName("type")
    @Expose
    private GeneralDetails type;

    public Types(String slot, GeneralDetails type) {
        this.slot = slot;
        this.type = type;
    }

    public String getSlot() {
        return slot;
    }

    public void setSlot(String slot) {
        this.slot = slot;
    }

    public GeneralDetails getType() {
        return type;
    }

    public void setType(GeneralDetails type) {
        this.type = type;
    }
}
