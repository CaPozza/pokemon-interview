package com.example.data.model;

import com.example.data.model.remote.Pokemon;

public class PokemonModel {

    private int count;
    private Next nextCall;
    private Pokemon pokemon;
    private PokemonDetail pokemonDetail;

    public PokemonModel(Next nextCall, Pokemon pokemon, PokemonDetail pokemonDetail, int count) {
        this.nextCall = nextCall;
        this.pokemon = pokemon;
        this.pokemonDetail = pokemonDetail;
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Next getNextCall() {
        return nextCall;
    }

    public void setNextCall(Next nextCall) {
        this.nextCall = nextCall;
    }

    public Pokemon getPokemon() {
        return pokemon;
    }

    public void setPokemon(Pokemon pokemon) {
        this.pokemon = pokemon;
    }

    public PokemonDetail getPokemonDetail() {
        return pokemonDetail;
    }

    public void setPokemonDetail(PokemonDetail pokemonDetail) {
        this.pokemonDetail = pokemonDetail;
    }
}
