package com.example.data.model.remote;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Species {
    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("url")
    @Expose
    private String speciesDetailUrl;

    public Species(String name, String speciesDetailUrl) {
        this.name = name;
        this.speciesDetailUrl = speciesDetailUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpeciesDetailUrl() {
        return speciesDetailUrl;
    }

    public void setSpeciesDetailUrl(String speciesDetailUrl) {
        this.speciesDetailUrl = speciesDetailUrl;
    }


}
