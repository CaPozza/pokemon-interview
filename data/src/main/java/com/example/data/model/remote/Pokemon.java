package com.example.data.model.remote;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pokemon {
    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("url")
    @Expose
    private String detailUrl;

    public Pokemon(String name, String url) {
        this.name = name;
        this.detailUrl = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return detailUrl;
    }

    public void setUrl(String url) {
        this.detailUrl = url;
    }
}
