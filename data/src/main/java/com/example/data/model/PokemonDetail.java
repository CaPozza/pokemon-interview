package com.example.data.model;


import com.example.data.model.remote.Species;
import com.example.data.model.remote.Stats;
import com.example.data.model.remote.Types;

import java.util.List;

public class PokemonDetail {

    private Sprites sprites;
    private Species species;
    private List<Stats> stats;
    private List<Types> type;

    public PokemonDetail(Sprites sprites, Species species, List<Stats> stats, List<Types> type) {
        this.sprites = sprites;
        this.species = species;
        this.stats = stats;
        this.type = type;
    }

    public Sprites getSpirites() {
        return sprites;
    }

    public void setSpirites(Sprites sprites) {
        this.sprites = sprites;
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public Sprites getSprites() {
        return sprites;
    }

    public void setSprites(Sprites sprites) {
        this.sprites = sprites;
    }

    public List<Stats> getStats() {
        return stats;
    }

    public void setStats(List<Stats> stats) {
        this.stats = stats;
    }

    public List<Types> getType() {
        return type;
    }

    public void setType(List<Types> type) {
        this.type = type;
    }
}
