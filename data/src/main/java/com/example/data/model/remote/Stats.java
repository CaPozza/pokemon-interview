package com.example.data.model.remote;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Stats {
    @SerializedName("base_stat")
    @Expose
    private int baseStat;

    @SerializedName("effort")
    @Expose
    private int effort;

    @SerializedName("stat")
    @Expose
    private GeneralDetails stat;

    public Stats(int baseStat, int effort, GeneralDetails stat) {
        this.baseStat = baseStat;
        this.effort = effort;
        this.stat = stat;
    }

    public int getBaseStat() {
        return baseStat;
    }

    public void setBaseStat(int baseStat) {
        this.baseStat = baseStat;
    }

    public int getEffort() {
        return effort;
    }

    public void setEffort(int effort) {
        this.effort = effort;
    }

    public GeneralDetails getStat() {
        return stat;
    }

    public void setStat(GeneralDetails stat) {
        this.stat = stat;
    }
}
