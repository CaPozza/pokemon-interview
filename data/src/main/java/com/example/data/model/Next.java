package com.example.data.model;

public class Next {
    public static final String OFFSET = "offset";
    public static final String LIMIT = "limit";
    public static final int STARTING_OFFSET = 0;
    public static final int LIMIT_VALUE = 45;

    private Integer offset;
    private Integer limit;

    public Next(Integer offset, Integer limit) {
        this.offset = offset;
        this.limit = limit;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }
}
