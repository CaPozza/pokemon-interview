package com.example.data.model;

import com.example.data.model.remote.SpritesOther;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Sprites {

    @SerializedName("other")
    @Expose
    private SpritesOther other;


    public Sprites(SpritesOther other) {
        this.other = other;
    }

    public SpritesOther getOther() {
        return other;
    }

    public void setOther(SpritesOther other) {
        this.other = other;
    }


}
