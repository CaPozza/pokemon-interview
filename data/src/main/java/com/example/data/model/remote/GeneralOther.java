package com.example.data.model.remote;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class GeneralOther {
    @SerializedName("front_default")
    @Expose
    private String frontDefaultImageUrl;

    public String getFrontDefaultImageUrl() {
        return frontDefaultImageUrl;
    }

    public void setFrontDefaultImageUrl(String frontDefaultImageUrl) {
        this.frontDefaultImageUrl = frontDefaultImageUrl;
    }
}