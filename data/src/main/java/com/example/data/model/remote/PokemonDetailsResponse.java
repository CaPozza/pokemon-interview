package com.example.data.model.remote;

import com.example.data.model.Sprites;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PokemonDetailsResponse {
    @SerializedName("sprites")
    @Expose
    private Sprites sprites;

    @SerializedName("species")
    @Expose
    private Species species;

    @SerializedName("types")
    @Expose
    private List<Types> types;

    @SerializedName("stats")
    @Expose
    private List<Stats> stats;

    public Sprites getSpirites() {
        return sprites;
    }

    public void setSpirites(Sprites sprites) {
        this.sprites = sprites;
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public Sprites getSprites() {
        return sprites;
    }

    public void setSprites(Sprites sprites) {
        this.sprites = sprites;
    }

    public List<Types> getTypes() {
        return types;
    }

    public void setTypes(List<Types> types) {
        this.types = types;
    }

    public List<Stats> getStats() {
        return stats;
    }

    public void setStats(List<Stats> stats) {
        this.stats = stats;
    }

}

