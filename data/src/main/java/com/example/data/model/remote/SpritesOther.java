package com.example.data.model.remote;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SpritesOther {

    @SerializedName("home")
    @Expose
    private GeneralOther home;

    @SerializedName("official-artwork")
    @Expose
    private GeneralOther officialArtWork;

    public GeneralOther getHome() {
        return home;
    }

    public void setHome(GeneralOther home) {
        this.home = home;
    }

    public GeneralOther getOfficialArtWork() {
        return officialArtWork;
    }

    public void setOfficialArtWork(GeneralOther officialArtWork) {
        this.officialArtWork = officialArtWork;
    }
}
