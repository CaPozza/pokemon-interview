
# Pokemon List

Project for Unicredit interview

# Plugin Used

Retrofit -> As Bonus request  
Stetho -> To Debug Network calls  
Reactive - AndroidRX - JavaRX -> to async call method system  
Picasso -> to render image from String URL

# API description

- **v2/pokemon/**: get pokemon list. To get pokemon list with Page method, a offset and limit must be inject as query parameters
- **v2/pokemon/{POKEMON_NAME}**: get pokemon details: app take care only to the following attribute: **types**, **stats** and **sprites for image**.

more details here: [PokeApi](https://pokeapi.co/)

# Project description
project based on MVVM and CLEAN architecture.
- **app**: this module is Android App, implements Master/Details fragment as interview request
- **data**: this module call the api and mapping results into interface for app.
