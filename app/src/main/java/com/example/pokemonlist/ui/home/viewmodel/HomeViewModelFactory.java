package com.example.pokemonlist.ui.home.viewmodel;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.pokemonlist.usecase.home.HomeUC;

public class HomeViewModelFactory implements ViewModelProvider.Factory {
    private HomeUC mParam;

    public HomeViewModelFactory(HomeUC param) {
        mParam = param;
    }


    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new HomeViewModel(mParam);
    }
}