package com.example.pokemonlist.ui.detail.adapter;

import android.view.View;
import android.widget.TextView;

import com.example.pokemonlist.R;
import com.example.pokemonlist.ui.base.BaseHolder;
import com.example.pokemonlist.ui.base.BaseUIModel;
import com.example.pokemonlist.ui.detail.model.StatsUIModel;

public class StatsHolder extends BaseHolder {
    private final TextView mEffortTextView, mBaseStatTextView, mStatNameTextView;

    public StatsHolder(View itemView) {
        super(itemView);
        mEffortTextView = itemView.findViewById(R.id.effort_pokemon);
        mBaseStatTextView = itemView.findViewById(R.id.base_stats_pokemon);
        mStatNameTextView = itemView.findViewById(R.id.pokemon_stats);
    }

    @Override
    public void bind(BaseUIModel model) {
        if(!(model instanceof StatsUIModel)){
           return;
        }
        final StatsUIModel statsUIModel = (StatsUIModel) model;
        if(mEffortTextView != null) mEffortTextView.setText(String.valueOf(statsUIModel.getEffort()));
        if(mBaseStatTextView != null) mBaseStatTextView.setText(String.valueOf(statsUIModel.getBaseStat()));
        if(mStatNameTextView != null) mStatNameTextView.setText(statsUIModel.getStatName());
    }
}
