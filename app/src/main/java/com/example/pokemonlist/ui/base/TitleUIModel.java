package com.example.pokemonlist.ui.base;

public class TitleUIModel extends BaseUIModel{

    String title;

    public TitleUIModel(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
