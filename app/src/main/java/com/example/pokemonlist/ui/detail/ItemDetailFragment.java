package com.example.pokemonlist.ui.detail;

import android.content.ClipData;
import android.content.Intent;
import android.os.Bundle;
import android.view.DragEvent;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.pokemonlist.R;
import com.example.pokemonlist.ui.detail.adapter.PokemonDetailAdapter;
import com.example.pokemonlist.ui.home.model.PokemonUIModel;
import com.example.pokemonlist.ui.utils.Utils;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.example.pokemonlist.databinding.FragmentItemDetailBinding;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ItemDetailFragment extends Fragment {

    public static final String ARG_ITEM_ID = "pokemon_id";
    private PokemonUIModel mPokemon;
    private CollapsingToolbarLayout mToolbarLayout;
    private RecyclerView mRecyclerView;
    private PokemonDetailAdapter mAdapter;


    private final View.OnDragListener dragListener = (v, event) -> {
        if (event.getAction() == DragEvent.ACTION_DROP) {
            ClipData.Item clipDataItem = event.getClipData().getItemAt(0);
            Intent intent = clipDataItem.getIntent();
            mPokemon = intent.getParcelableExtra(ARG_ITEM_ID);
            updateContent();
        }
        return true;
    };
    private FragmentItemDetailBinding binding;
    private ImageView mPokemonImage;

    public ItemDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        assert getArguments() != null;
        if (getArguments().containsKey(ARG_ITEM_ID)) {
            mPokemon = getArguments().getParcelable(ARG_ITEM_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentItemDetailBinding.inflate(inflater, container, false);
        final View rootView = binding.getRoot();

        mToolbarLayout = rootView.findViewById(R.id.toolbar_layout);
        if(mToolbarLayout != null) mPokemonImage = mToolbarLayout.findViewById(R.id.pokemon_image);
        mRecyclerView = binding.detailItemList;
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(
                getContext(),
                LinearLayoutManager.VERTICAL,
                false
        );
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mAdapter = new PokemonDetailAdapter(new ArrayList<>(), getContext());
        mRecyclerView.setAdapter(mAdapter);

        updateContent();
        rootView.setOnDragListener(dragListener);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private void updateContent() {
        if(mPokemon == null){
            return;
        }

        if(mToolbarLayout != null){
            mToolbarLayout.setTitle(Utils.capitalizeFirstLetter(mPokemon.getName()));
        }

        if (mPokemonImage != null){
            Picasso.get().load(mPokemon.getDetailImage()).into(mPokemonImage);
        }

        mAdapter.setTypeList(mPokemon.getTypeUIModel());
        mAdapter.setStatList(mPokemon.getStatsUIModel());
    }


}