package com.example.pokemonlist.ui.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;

import com.example.pokemonlist.observer.ConnectivityObserver;

public class ConnectivityReceiver extends BroadcastReceiver {


    private ConnectivityObserver mNetworkObserver;

    public ConnectivityReceiver(ConnectivityObserver mNetworkObserver) {
        this.mNetworkObserver = mNetworkObserver;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        ConnectivityManager connectivityManager =
                ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        final boolean connected = connectivityManager.getActiveNetworkInfo() != null
                && connectivityManager.getActiveNetworkInfo().isConnected();

        if(mNetworkObserver != null){
            mNetworkObserver.onConnectionChange(connected);
        }

    }

}
