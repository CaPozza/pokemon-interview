package com.example.pokemonlist.ui.detail.model;
import android.os.Parcel;
import android.os.Parcelable;

import com.example.pokemonlist.ui.base.BaseUIModel;

public class TypeUIModel extends BaseUIModel implements Parcelable {
    private final String type;

    public TypeUIModel(String type) {
        this.type = type;
    }

    protected TypeUIModel(Parcel in) {
        type = in.readString();
    }

    public static final Creator<TypeUIModel> CREATOR = new Creator<TypeUIModel>() {
        @Override
        public TypeUIModel createFromParcel(Parcel in) {
            return new TypeUIModel(in);
        }

        @Override
        public TypeUIModel[] newArray(int size) {
            return new TypeUIModel[size];
        }
    };

    public String getType() {
        return type;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(type);
    }
}

