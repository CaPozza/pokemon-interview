package com.example.pokemonlist.ui.home.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.data.model.Next;
import com.example.pokemonlist.ui.detail.model.StatsUIModel;
import com.example.pokemonlist.ui.detail.model.TypeUIModel;

import java.util.List;

public class PokemonUIModel  implements Parcelable {
    private final String name;
    private final String miniMap;
    private final String detailImage;
    private final Next nextPokemonList;
    private final List<StatsUIModel> statsUIModel;
    private final List<TypeUIModel> typeUIModel;

    private boolean hasMore;

    public PokemonUIModel(String n,
                          String u,
                          String detailImage,
                          Next nextList,
                          List<StatsUIModel> statsUIModel,
                          List<TypeUIModel> typeUIModel){
        this.name = n;
        this.miniMap = u;
        this.detailImage = detailImage;
        this.nextPokemonList = nextList;
        this.statsUIModel = statsUIModel;
        this.typeUIModel = typeUIModel;
    }


    public List<StatsUIModel> getStatsUIModel() {
        return statsUIModel;
    }

    public List<TypeUIModel> getTypeUIModel() {
        return typeUIModel;
    }

    public String getName() {
        return name;
    }

    public Next getNextPokemonList() {
        return nextPokemonList;
    }

    public boolean isHasMore() {
        return hasMore;
    }

    public void setHasMore(boolean hasMore) {
        this.hasMore = hasMore;
    }

    public String getMiniMap() {
        return miniMap;
    }

    public String getDetailImage() {
        return detailImage;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(miniMap);
        dest.writeString(detailImage);
        dest.writeByte((byte) (hasMore ? 1 : 0));
        dest.writeInt(nextPokemonList.getOffset());
        dest.writeInt(nextPokemonList.getLimit());
    }

    @Override
    public int describeContents() {
        return 0;
    }


    public static final Creator<PokemonUIModel> CREATOR = new Creator<PokemonUIModel>() {

        @Override
        public PokemonUIModel createFromParcel(Parcel parcel) {
            return null;
        }

        @Override
        public PokemonUIModel[] newArray(int size) {
            return new PokemonUIModel[size];
        }
    };


}
