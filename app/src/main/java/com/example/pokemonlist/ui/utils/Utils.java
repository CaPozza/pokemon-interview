package com.example.pokemonlist.ui.utils;

import android.content.Context;

import com.example.pokemonlist.ui.base.TitleUIModel;

import java.util.ArrayList;
import java.util.List;

public class Utils {
    public static String capitalizeFirstLetter(String str) {
        return str.substring(0, 1).toUpperCase() + str.substring(1).toLowerCase();
    }

    public static List<TitleUIModel> getTitleAsUIModel(int type, Context context) {
        final List<TitleUIModel> r = new ArrayList<>();
        r.add(new TitleUIModel(context.getString(type)));
        return r;
    }
}
