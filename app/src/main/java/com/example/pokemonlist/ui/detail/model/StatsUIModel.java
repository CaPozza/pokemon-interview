package com.example.pokemonlist.ui.detail.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.pokemonlist.ui.base.BaseUIModel;

public class StatsUIModel extends BaseUIModel implements Parcelable {

    private final int baseStat;
    private final int effort;
    private final String StatName;

    public StatsUIModel(int baseStat, int effort, String statName) {
        this.baseStat = baseStat;
        this.effort = effort;
        StatName = statName;
    }

    protected StatsUIModel(Parcel in) {
        baseStat = in.readInt();
        effort = in.readInt();
        StatName = in.readString();
    }

    public static final Creator<StatsUIModel> CREATOR = new Creator<StatsUIModel>() {
        @Override
        public StatsUIModel createFromParcel(Parcel in) {
            return new StatsUIModel(in);
        }

        @Override
        public StatsUIModel[] newArray(int size) {
            return new StatsUIModel[size];
        }
    };

    public int getBaseStat() {
        return baseStat;
    }

    public int getEffort() {
        return effort;
    }

    public String getStatName() {
        return StatName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(baseStat);
        parcel.writeInt(effort);
        parcel.writeString(StatName);
    }
}
