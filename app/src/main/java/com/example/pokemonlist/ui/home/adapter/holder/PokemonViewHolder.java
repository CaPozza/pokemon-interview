package com.example.pokemonlist.ui.home.adapter.holder;

import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.pokemonlist.databinding.ItemListContentBinding;

class PokemonViewHolder extends RecyclerView.ViewHolder {
    final TextView mPokemonName;
    final ImageView mPokemonimage;

    PokemonViewHolder(ItemListContentBinding binding) {
        super(binding.getRoot());

        mPokemonName = binding.namePokemon;
        mPokemonimage = binding.miniMapPokemon;
    }

}