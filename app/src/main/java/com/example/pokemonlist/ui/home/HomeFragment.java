package com.example.pokemonlist.ui.home;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.pokemonlist.R;
import com.example.pokemonlist.databinding.FragmentItemListBinding;

import com.example.pokemonlist.ui.home.adapter.holder.PokemonListAdapter;
import com.example.pokemonlist.ui.home.model.ListPokemonUIModel;
import com.example.pokemonlist.ui.home.viewmodel.HomeViewModel;
import com.example.pokemonlist.ui.home.viewmodel.HomeViewModelFactory;
import com.example.pokemonlist.ui.utils.EndlessRecyclerOnScrollListener;
import com.example.pokemonlist.usecase.home.HomeUC;

import java.util.ArrayList;

public class HomeFragment extends Fragment{

    private FragmentItemListBinding binding;
    private HomeViewModel mHomeViewModel;

    private LoadMore mLoadMore;
    private View mItemDetailFragmentContainer;
    private RecyclerView mRecyclerView;
    private PokemonListAdapter mAdapter;
    private final static String TAG = "HomeFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentItemListBinding.inflate(inflater, container, false);
        mHomeViewModel =  new ViewModelProvider(this,
                new HomeViewModelFactory(
                        HomeUC.getInstance(getContext()))).get(HomeViewModel.class);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRecyclerView = binding.itemList;
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mLoadMore = new LoadMore(linearLayoutManager);
        mRecyclerView.setLayoutManager(linearLayoutManager);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        mItemDetailFragmentContainer = view.findViewById(R.id.item_detail_nav_container);
        mAdapter = new PokemonListAdapter(
                new ArrayList<>(),
                mItemDetailFragmentContainer
        );
        mRecyclerView.setAdapter(mAdapter);

        Observer<ListPokemonUIModel> listPokemonUIModelObserver = list -> {
            updatePokemonList();
        };
        mHomeViewModel.getListPokemonUIModel().observe(getActivity(), listPokemonUIModelObserver);
        mHomeViewModel.loadNextPagePokemon();

        mRecyclerView.addOnScrollListener(mLoadMore);

    }

    private void updatePokemonList() {
        final ListPokemonUIModel listPokemonUIModel = mHomeViewModel.getListPokemonUIModel().getValue();
        if(listPokemonUIModel != null){
            mAdapter.submitList(listPokemonUIModel.getPokemonUIModels());
            mLoadMore.loadDone(listPokemonUIModel.hasMore());
            return;
        }

        mLoadMore.loadDone(listPokemonUIModel.hasMore());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private class LoadMore extends EndlessRecyclerOnScrollListener {
        LoadMore(LinearLayoutManager layoutManager) {
            super(layoutManager);
        }

        @Override
        public void onLoadMore() {
            mHomeViewModel.loadNextPagePokemon();
        }
    }
}