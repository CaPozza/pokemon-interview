package com.example.pokemonlist.ui.base;

import android.view.View;
import android.widget.TextView;

import com.example.pokemonlist.R;

public class TitleHolder extends BaseHolder {
    private final TextView mTitleTextView;

    public TitleHolder(View itemView) {
        super(itemView);
        mTitleTextView = itemView.findViewById(R.id.title_section);
    }

    @Override
    public void bind(BaseUIModel model) {
        if(!(model instanceof TitleUIModel)){
            return;
        }
        final TitleUIModel typeUIModel = (TitleUIModel) model;
        mTitleTextView.setText(typeUIModel.getTitle());
    }
}
