package com.example.pokemonlist.ui.home.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.data.model.Next;
import com.example.pokemonlist.observer.SingleObserverProgress;
import com.example.pokemonlist.ui.home.model.ListPokemonUIModel;
import com.example.pokemonlist.ui.home.model.PokemonUIModel;
import com.example.pokemonlist.usecase.home.HomeUC;

import java.util.LinkedList;

import io.reactivex.android.schedulers.AndroidSchedulers;

public class HomeViewModel extends ViewModel {
    private HomeUC mHome;
    private MutableLiveData<ListPokemonUIModel> mPokemonList = new MutableLiveData<>();

    public HomeViewModel(HomeUC homeUC) {
        mHome = homeUC;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
    }

    public LiveData<ListPokemonUIModel> getListPokemonUIModel() {
        return mPokemonList;
    }


    public void loadNextPagePokemon() {
        ListPokemonUIModel pokemonList = mPokemonList.getValue();
        Next next;
        if (pokemonList == null) {
            pokemonList = new ListPokemonUIModel(new LinkedList<>(), true, 0);
            next = new Next(Next.STARTING_OFFSET, Next.LIMIT_VALUE);
        }else {
            final PokemonUIModel lastPokemonOfPage =
                    pokemonList.getPokemonUIModels().get(pokemonList.getPokemonUIModels().size()-1);

            next = new Next(lastPokemonOfPage.getNextPokemonList().getOffset(),
                    lastPokemonOfPage.getNextPokemonList().getLimit());
        }

        if(pokemonList.hasMore()){
            mHome.getPokemonList(next, pokemonList)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserverProgress<ListPokemonUIModel>() {
                        @Override
                        public void accept(ListPokemonUIModel pokemons) {
                            mPokemonList.setValue(pokemons);
                        }
                    });
        }
    }
}
