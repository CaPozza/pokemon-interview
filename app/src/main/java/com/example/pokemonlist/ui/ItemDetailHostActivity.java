package com.example.pokemonlist.ui;

import android.animation.Animator;
import android.app.AlertDialog;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.pokemonlist.BuildConfig;
import com.example.pokemonlist.R;
import com.example.pokemonlist.databinding.ActivityItemDetailBinding;
import com.example.pokemonlist.observer.ConnectivityObserver;
import com.example.pokemonlist.observer.TemplateViewModel;
import com.example.pokemonlist.observer.TemplateViewModelFactory;
import com.example.pokemonlist.ui.utils.ConnectivityReceiver;
import com.facebook.stetho.Stetho;
import com.squareup.picasso.Picasso;

public class ItemDetailHostActivity extends AppCompatActivity implements ConnectivityObserver {

    private View progressContainer;
    private TemplateViewModel mTemplateViewModel;
    private static final String TAG = "ItemDetailHostActivity";
    private boolean progressIsShowing = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (BuildConfig.DEBUG){
            Stetho.initializeWithDefaults(this);
            Picasso.get().setLoggingEnabled(false);
        }


        ActivityItemDetailBinding binding = ActivityItemDetailBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager()
                .findFragmentById(R.id.nav_host_fragment_item_detail);
        NavController navController = navHostFragment.getNavController();
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.
                Builder(navController.getGraph())
                .build();

        progressContainer = findViewById(R.id.progressContainer);
        progressContainer.setAlpha(0f); //hide progress on boot

        if(mTemplateViewModel == null){
            mTemplateViewModel = new ViewModelProvider(this, new TemplateViewModelFactory()).get(TemplateViewModel.class);
        }
        mTemplateViewModel.getProgress().observe(this, visible -> {
            Log.d(TAG, "Is progress showing?" + visible);
            if (visible != null && visible) showProgress();
            else hideProgress();
        });

        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);

        registerReceiver(
                new ConnectivityReceiver(this),
                new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        );
    }

    private void showProgress() {
        if (progressIsShowing) {
            // quando 2 observable partono in successione bisogna cancellare l'animazione di chiusura in corso
            if (progressContainer != null) {
                Log.w(TAG, "- PROGRESS -progress just show: clear animation");
                if(progressContainer != null)
                progressContainer.animate().cancel();
            }
        }

        Log.w("TAG", "- PROGRESS - show startAnim");
        progressIsShowing = true;
        if(progressContainer != null){
            progressContainer.setVisibility(View.VISIBLE);
            progressContainer.animate()
                    .alpha(1f)
                    .setDuration(400)
                    .setListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animator) {
                            Log.w(TAG, "- PROGRESS - start show");
                        }

                        @Override
                        public void onAnimationEnd(Animator animator) {
                            Log.w(TAG, "- PROGRESS - end show");
                        }

                        @Override
                        public void onAnimationCancel(Animator animator) {
                            Log.w(TAG, "- PROGRESS - cancel show");
                        }

                        @Override
                        public void onAnimationRepeat(Animator animator) {

                        }
                    });
        }
    }

    private void hideProgress() {
        Log.w(TAG, "- PROGRESS - hide progressIsShowing=$progressIsShowing");
        if (progressIsShowing) {
            if (progressContainer != null) {
                Log.w(TAG, "- PROGRESS - hide startAnimation");
                progressContainer.animate()
                        .alpha(0f)
                        .setDuration(600)
                        .setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animator) {
                                Log.w(TAG, "- PROGRESS - start hide");
                            }

                            @Override
                            public void onAnimationEnd(Animator animator) {
                                Log.w(TAG, "- PROGRESS -  end hide");
                                progressContainer.setVisibility(View.GONE);
                                progressIsShowing = false;
                            }

                            @Override
                            public void onAnimationCancel(Animator animator) {
                                Log.w(TAG, "- PROGRESS - cancel hide");
                            }

                            @Override
                            public void onAnimationRepeat(Animator animator) {

                            }
                        });
                progressContainer.setOnClickListener(null);
                return;
            }
                Log.w(TAG, "No progress-layout is present to hide");
        }
    }
    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_item_detail);
        return navController.navigateUp() || super.onSupportNavigateUp();
    }

    @Override
    public void onStart(){
        super.onStart();
        mTemplateViewModel = new ViewModelProvider(this, new TemplateViewModelFactory()).get(TemplateViewModel.class);
        mTemplateViewModel.getErrors().observe(this, throwable -> {
            Log.e(TAG, throwable.getMessage());
        });
    }

    @Override
    public void onConnectionChange(boolean isConnected) {
        Log.i(TAG, "onConnectionChange() " + isConnected);
        if(!isConnected){
            final AlertDialog.Builder builder = new AlertDialog.Builder(ItemDetailHostActivity.this);
            builder.setMessage(R.string.dialog_no_internet);
            builder.create().show();
        }
    }
}