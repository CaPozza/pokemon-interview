package com.example.pokemonlist.ui.home.adapter.holder;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pokemonlist.R;
import com.example.pokemonlist.databinding.ItemListContentBinding;
import com.example.pokemonlist.ui.detail.ItemDetailFragment;
import com.example.pokemonlist.ui.home.model.PokemonUIModel;
import com.example.pokemonlist.ui.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;


public class PokemonListAdapter
        extends RecyclerView.Adapter<PokemonViewHolder> {

    private List<PokemonUIModel> pokemon;
    private final View mItemDetailFragmentContainer;
    private int selected=-1;


    public PokemonListAdapter(final List<PokemonUIModel> p,
                              final View itemDetailFragmentContainer) {
        pokemon = p;
        mItemDetailFragmentContainer = itemDetailFragmentContainer;
    }

    @Override
    public PokemonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        ItemListContentBinding binding =
                ItemListContentBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new PokemonViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(final PokemonViewHolder holder, int position) {
        if(selected == position)
            holder.itemView.setBackgroundColor(Color.parseColor("#99ef5350"));
        else
            holder.itemView.setBackgroundColor(Color.parseColor("#ffffff"));

        final PokemonUIModel currentPokemon = pokemon.get(position);
        final String name = position + ". " + Utils.capitalizeFirstLetter(currentPokemon.getName());
        Picasso.get()
                .load(currentPokemon.getMiniMap())
                .resize(150,150)
                .into(holder.mPokemonimage);

        holder.mPokemonName.setText(name);
        holder.itemView.setTag(currentPokemon);
        holder.itemView.setOnClickListener(itemView -> {
            selected=position;
            holder.itemView.setBackgroundColor(Color.parseColor("#99ef5350"));
            Bundle arguments = new Bundle();
            arguments.putParcelable(ItemDetailFragment.ARG_ITEM_ID, currentPokemon);
            if (mItemDetailFragmentContainer != null) {
                Navigation.findNavController(mItemDetailFragmentContainer)
                        .navigate(R.id.fragment_item_detail, arguments);
            } else {
                Navigation.findNavController(itemView).navigate(R.id.show_item_detail, arguments);
            }
            notifyDataSetChanged();
        });
    }

    @Override
    public int getItemCount() {
        return pokemon.size();
    }

    public void submitList(List<PokemonUIModel> pokemonUIModel) {
        final int startingRange = pokemon.size();
        pokemon = pokemonUIModel;
        this.notifyItemRangeInserted(startingRange,pokemon.size());

    }
}
