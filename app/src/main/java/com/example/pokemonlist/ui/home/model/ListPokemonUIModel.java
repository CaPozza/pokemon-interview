package com.example.pokemonlist.ui.home.model;

import java.util.List;

public class ListPokemonUIModel {
    private List<PokemonUIModel> pokemonUIModels;
    private boolean more;
    private int count;

    public ListPokemonUIModel(List<PokemonUIModel> pokemonUIModels, boolean hasMore, int count) {
        this.pokemonUIModels = pokemonUIModels;
        more = hasMore;
        this.count = count;
    }

    public List<PokemonUIModel> getPokemonUIModels() {
        return pokemonUIModels;
    }

    public void setPokemonUIModels(List<PokemonUIModel> pokemonUIModels) {
        this.pokemonUIModels = pokemonUIModels;
    }

    public boolean hasMore() {
        return more;
    }

    public void setHasMore(boolean hasMore) {
        more = hasMore;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
