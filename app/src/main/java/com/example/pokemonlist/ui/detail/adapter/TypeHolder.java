package com.example.pokemonlist.ui.detail.adapter;

import android.view.View;
import android.widget.TextView;

import com.example.pokemonlist.R;
import com.example.pokemonlist.ui.base.BaseHolder;
import com.example.pokemonlist.ui.base.BaseUIModel;
import com.example.pokemonlist.ui.detail.model.TypeUIModel;

public class TypeHolder extends BaseHolder {
    private final TextView mTypeTextView;
    public TypeHolder(View itemView) {
        super(itemView);

        mTypeTextView = itemView.findViewById(R.id.pokemon_type);
    }

    @Override
    public void bind(BaseUIModel model) {
        if(!(model instanceof TypeUIModel)){
            return;
        }
        final TypeUIModel typeUIModel = (TypeUIModel) model;
        mTypeTextView.setText(typeUIModel.getType());
    }
}
