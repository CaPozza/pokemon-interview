package com.example.pokemonlist.ui.detail.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pokemonlist.R;
import com.example.pokemonlist.ui.base.BaseHolder;
import com.example.pokemonlist.ui.base.BaseUIModel;
import com.example.pokemonlist.ui.base.TitleHolder;
import com.example.pokemonlist.ui.base.TitleUIModel;
import com.example.pokemonlist.ui.detail.model.StatsUIModel;
import com.example.pokemonlist.ui.detail.model.TypeUIModel;
import com.example.pokemonlist.ui.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class PokemonDetailAdapter extends RecyclerView.Adapter<BaseHolder> {

    private final int mViewTypePokemonStat = 0;
    private final int mViewTypePokemonType = 1;
    private final int mViewTypeTitle = 2;

    private StatsUIModel statsUIModel = null;
    private TypeUIModel typeUIModel = null;
    private TitleUIModel titleUiModel = null;

    private List<TitleUIModel> titlelList = new ArrayList<>();
    private List<TypeUIModel> pokemonTypeList = new ArrayList<>();
    private List<StatsUIModel> pokemonStatList = new ArrayList<>();

    private List<BaseUIModel> list;
    private final Context mContext;


    public PokemonDetailAdapter(final List<BaseUIModel> l, final Context context) {
        list = l;
        mContext = context;
    }

    @NonNull
    @Override
    public BaseHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == mViewTypePokemonStat ) {
            return new StatsHolder(LayoutInflater.from(parent.getContext())
                    .inflate(
                            R.layout.pokemon_stat_widget,
                            parent,
                            false)
            );
        }

        if(viewType == mViewTypeTitle){
            return new TitleHolder(LayoutInflater.from(parent.getContext())
                    .inflate(
                            R.layout.title_widget,
                            parent,
                            false)
            );
        }

        return new TypeHolder(LayoutInflater.from(parent.getContext())
                .inflate(
                        R.layout.pokemon_type_widget,
                        parent,
                        false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull BaseHolder holder, int position) {
        holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(list.get(position) instanceof TypeUIModel){
            return mViewTypePokemonType;
        }
        if(list.get(position) instanceof TitleUIModel){
            return mViewTypeTitle;
        }
        return mViewTypePokemonStat;
    }

    public void setTypeList(List<TypeUIModel> type) {
        this.pokemonTypeList = type;
        update();
    }

    public void setStatList(List<StatsUIModel> stat){
        this.pokemonStatList = stat;
        update();
    }

    public void setTitleList(List<TitleUIModel> list){
        this.titlelList = list;
        update();
    }

    @SuppressLint("NotifyDataSetChanged")
    private void update() {
        list.clear();
        list.addAll(Utils.getTitleAsUIModel(R.string.type, mContext));
        list.addAll(this.pokemonTypeList);
        list.addAll(Utils.getTitleAsUIModel(R.string.stat, mContext));
        list.addAll(this.pokemonStatList);

        notifyDataSetChanged();
    }

}
