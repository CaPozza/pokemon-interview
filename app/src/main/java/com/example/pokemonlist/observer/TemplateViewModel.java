package com.example.pokemonlist.observer;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class TemplateViewModel extends ViewModel {

    // unique progress of activity
    private static MutableLiveData<Boolean> progress = new MutableLiveData<>();

    protected static ProgressManager progressManager;
    // errors
    protected static ProgressLiveEvent<Throwable> errors = new ProgressLiveEvent<>();

    public MutableLiveData<Boolean> getProgress() {
        if (progressManager == null) {
            progressManager = new ProgressManager(progress);
        }
        return progress;
    }

    public ProgressLiveEvent<Throwable> getErrors() {
        return errors;
    }
}
