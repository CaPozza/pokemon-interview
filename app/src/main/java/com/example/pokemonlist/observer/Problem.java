package com.example.pokemonlist.observer;

public interface Problem {
    Throwable onProblem(Throwable e);
}
