package com.example.pokemonlist.observer;

public interface ConnectivityObserver {
    public void onConnectionChange(boolean isConnected);
}
