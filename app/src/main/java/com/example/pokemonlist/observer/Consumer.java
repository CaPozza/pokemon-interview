package com.example.pokemonlist.observer;

public interface Consumer<T> {
    void accept(T t);
}
