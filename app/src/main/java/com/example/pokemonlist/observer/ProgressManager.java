package com.example.pokemonlist.observer;
import android.os.Looper;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

public class ProgressManager {

    public static final String TAG = "ProgressManager";
    protected MutableLiveData<Boolean> progress;
    private int counter = 0;

    public ProgressManager(MutableLiveData<Boolean> progress) {
        this.progress = progress;
    }

    public synchronized void show() {
        counter++;
        checkCounter();
    }

    public synchronized void hide() {
        counter--;
        if (counter < 0) {
            counter = 0;
            Log.e(TAG, "ProgressManager should never happen!!");
        }
        checkCounter();
    }

    private void checkCounter() {
        if (counter == 0) {
            if (isMainThread()) {
                progress.setValue(false);
            } else {
                progress.postValue(false);
            }
        } else if (counter == 1) {
            if (isMainThread()) {
                progress.setValue(true);
            } else {
                progress.postValue(true);
            }
        }
    }

    private boolean isMainThread() {
        boolean isMainThread = Looper.myLooper() == Looper.getMainLooper();
        Log.w("ProgressManager", "isMainThread? " + isMainThread);
        return isMainThread;
    }
}

