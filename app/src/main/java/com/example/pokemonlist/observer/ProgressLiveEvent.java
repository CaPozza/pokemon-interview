package com.example.pokemonlist.observer;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.annotation.MainThread;
import androidx.annotation.Nullable;
import android.util.Log;

import com.example.pokemonlist.BuildConfig;

import java.util.concurrent.atomic.AtomicBoolean;

public class ProgressLiveEvent<T> extends MutableLiveData<T> {

    private static final String TAG = "ProgressLiveEvent";

    private final AtomicBoolean mPending = new AtomicBoolean(false);

    private Observer<? super T> current;

    @MainThread
    @Override
    public void observe(@NonNull LifecycleOwner owner, @NonNull Observer<? super T> observer) {
        // added for multiple activity
        if (current != null) {
            removeObserver(current);
        }

        if (hasObservers()) {
            Log.w(TAG, "Multiple observers registered but only one will be notified of changes.");
            if (BuildConfig.DEBUG) {
                throw new RuntimeException("Multiple observers registered but only one will be notified of changes.");
            }
        }

        current = new Observer<T>() {
            @Override
            public void onChanged(@Nullable T t) {
                if (mPending.compareAndSet(true, false)) {
                    observer.onChanged(t);
                }
            }
        };
        // Observe the internal MutableLiveData
        super.observe(owner, current);
    }

    @MainThread
    public void setValue(@Nullable T t) {
        mPending.set(true);
        super.setValue(t);
    }

}

