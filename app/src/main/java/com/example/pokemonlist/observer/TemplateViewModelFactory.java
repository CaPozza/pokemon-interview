package com.example.pokemonlist.observer;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.pokemonlist.ui.home.viewmodel.HomeViewModel;
import com.example.pokemonlist.usecase.home.HomeUC;


public class TemplateViewModelFactory implements ViewModelProvider.Factory {

    public TemplateViewModelFactory() {
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new TemplateViewModel();
    }
}