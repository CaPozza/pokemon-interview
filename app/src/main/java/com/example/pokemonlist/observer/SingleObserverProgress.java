package com.example.pokemonlist.observer;
import android.util.Log;

import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

public abstract class SingleObserverProgress<T> implements SingleObserver<T>, Consumer<T>, Problem {

    protected final ProgressManager progressManager;
    protected final ProgressLiveEvent<Throwable> errors;

    public SingleObserverProgress() {
        this.progressManager = TemplateViewModel.progressManager;
        this.errors = TemplateViewModel.errors;
    }

    @Override
    public void onSubscribe(Disposable d) {
        if (progressManager != null) {
            progressManager.show();
        }
    }

    @Override
    public void onSuccess(T t) {
        if (progressManager != null) {
            progressManager.hide();
        }
        accept(t);
    }

    @Override
    public void onError(Throwable e) {
        Log.e(getClass().getSimpleName(), "onError ", e);
        if (progressManager != null) {
            progressManager.hide();
        }

        Throwable t = onProblem(e);
        if (t != null) {
            errors.postValue(t);
        }
    }

    @Override
    public Throwable onProblem(Throwable e) {
        return e;
    }
}
