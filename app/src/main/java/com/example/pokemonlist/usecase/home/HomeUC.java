package com.example.pokemonlist.usecase.home;

import android.annotation.SuppressLint;
import android.content.Context;

import com.example.data.model.Next;
import com.example.data.model.PokemonModel;
import com.example.data.model.remote.Stats;
import com.example.data.model.remote.Types;
import com.example.data.repository.PokemonRepository;
import com.example.pokemonlist.ui.detail.model.StatsUIModel;
import com.example.pokemonlist.ui.detail.model.TypeUIModel;
import com.example.pokemonlist.ui.home.model.ListPokemonUIModel;
import com.example.pokemonlist.ui.home.model.PokemonUIModel;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import io.reactivex.Single;

public class HomeUC {
    private final PokemonRepository mPokemonRepository;


    private static HomeUC INSTANCE;

    public static HomeUC getInstance( Context context){
        if(INSTANCE == null){
            INSTANCE = new HomeUC(context);
        }

        return INSTANCE;
    }


    public HomeUC(Context context) {
        mPokemonRepository = PokemonRepository.getInstance(context);
    }

    @SuppressLint("CheckResult")
    public Single<ListPokemonUIModel> getPokemonList(@Nullable Next next, ListPokemonUIModel prevList) {
        if(next == null) {
            return Single.just(new ListPokemonUIModel(new ArrayList(), false, prevList.getCount()));
        }
        return mPokemonRepository.getPokemonList(next).flatMap( newList -> {
            if(prevList.getCount() ==  newList.get(0).getCount()){
                return Single.just(new ListPokemonUIModel(new ArrayList(), false, prevList.getCount()));
            }else {
                final List<PokemonUIModel> pokemonList = prevList.getPokemonUIModels();
                pokemonList.addAll(mappingPokemonModel(newList).getPokemonUIModels());
                return Single.just(new ListPokemonUIModel(pokemonList,
                        pokemonList.size() < newList.get(0).getCount(),
                        pokemonList.size()));
            }

        });
    }

    public ListPokemonUIModel mappingPokemonModel(List<PokemonModel> pokemonList) {
        final List<PokemonUIModel> list = new ArrayList<>();
        for (PokemonModel p : pokemonList) {
            list.add(new PokemonUIModel(
                    p.getPokemon().getName(),
                    p.getPokemonDetail().getSpirites().getOther()
                            .getHome().getFrontDefaultImageUrl(),
                    p.getPokemonDetail().getSpirites().getOther()
                                    .getOfficialArtWork().getFrontDefaultImageUrl(),
                    p.getNextCall(),
                    mappingPokemonStatDetails(p),
                    mappingPokemonTypeDetails(p)
                    ));
        }

        return new ListPokemonUIModel(list,
                list.get(list.size()-1).getNextPokemonList() != null,
                list.size());
    }

    private List<TypeUIModel> mappingPokemonTypeDetails(PokemonModel p) {
        if(p.getPokemonDetail().getType().size() == 0){
            return new ArrayList<>();
        }

        final List<TypeUIModel> type = new ArrayList<>();
        for (Types t :  p.getPokemonDetail().getType()) {
            type.add(new TypeUIModel(
                    t.getType().getName()
            ));
        }
        return type;
    }

    private List<StatsUIModel> mappingPokemonStatDetails(PokemonModel p) {
        if(p.getPokemonDetail().getStats().size() == 0){
            return new ArrayList<>();
        }
        final List<StatsUIModel> stats = new ArrayList<>();
        for (Stats s :  p.getPokemonDetail().getStats()) {
            stats.add(new StatsUIModel(
                    s.getBaseStat(),
                    s.getEffort(),
                    s.getStat().getName()
            ));
        }
        return stats;
    }
}
